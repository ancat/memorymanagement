public class Debug {
	public static boolean enabled = false;
	public static void print(String debugstatement) {
		if (enabled) {
			System.out.println("DEBUG: " + debugstatement);
		}
	}
}
/**
 *  The process object.
 */

import java.util.ArrayList;

public class Process {
    public int pid;
    public int pageSize;
    public int numPages;
    public ArrayList<MemoryReference> references;
    public PageTable pageTable;

    public Process(int pid, int pageSize, int numPages, ArrayList<MemoryReference> references) {
        this.pid = pid;
        this.pageSize = pageSize;
        this.numPages = numPages;
        this.references = references;
        this.pageTable = new PageTable(pageSize, numPages);

        Debug.print(String.format("Pid: %d; References: %d; Pages: %d; Page Size: %d", pid, references.size(), numPages, pageSize));
    }

    public boolean done() {
        return this.references.size() == 0;
    }

    public boolean run() {
        if (this.done()) {
            return false;
        }
        this.references.remove(0);
        return true;
    }
}
import java.util.*;
public class mmu {
	//This thing is barely like an mmu, as all of the resolving is done within other classes
	//It only has 3 externally interfacing methods (endProcess, read and write), there is also the constructor too 
	//ArrayList<ProcessControlBlock> block;
	ProcessControlBlock globalBlock; //For Process information
	MemoryMap memory; //Model of memory 
	int currentFrame;
	int removalTick; //Our hand
	int maximumFrames;
	ArrayList<Integer> freeFrames; 
	ArrayList<Integer> clock;
	int pageSize;
	int pageNumber;
	int maxPages;
	int globalBlockCrusor;
	int penalty;
	boolean debugOutput;
	public mmu(int pageSize, int maxPages, int maximumFrames, boolean debugOutput){
		this.pageSize = pageSize;
		this.maxPages = maxPages;
		this.maximumFrames = maximumFrames; 
		for(int i = 0 ; i < maximumFrames; i++){
			freeFrames.add(i);
		}
		this.debugOutput = debugOutput;
		this.currentFrame = 0;
		this.removalTick = 0 ; 
		this.globalBlockCrusor = 0 ; 
	}
	public int pageType (int pid, int virtualAddress){
		pageTableEntry p = null;
		ArrayList<Integer> tmp =  globalBlock.pointerTable.get(pid); 
		for(int i =0 ; i < tmp.size(); ++i){
			if(virtualAddress== memory.entryTables.get(tmp.get(i)).virtualAddress){
				p = memory.entryTables.get(tmp.get(i));
				break;
			}
		}
		if(p == null)return -1;//free
		else{
			if(p.valid)return 0 ;//hit
			else if(!p.valid && !p.dirty)return 1; //clean
			else if(!p.valid &&  p.dirty)return 2; //Dirty
			else if(!p.valid &&  p.busy)return 3; //Still under Penalty 
			else return 4; //Wut
		}
	}
	private void incrementFrames(){
		this.currentFrame ++;
		if(this.currentFrame > this.maximumFrames-1){
			this.currentFrame = 0 ;
			return;
		}
		Integer noTypeConfusionpls = this.currentFrame;
		this.freeFrames.set(noTypeConfusionpls-1, -1);
	}
	private void incrementPageNumber(){
		this.pageNumber ++;
		if(this.pageNumber > this.maxPages){
			this.pageNumber = 0 ; 
		}
	}
	private void setPenalty(int pid, int index){
		memory.entryTables.get(index).penalty = this.penalty;
	}
	private int calculateOffset(int virtualAddress){
		return virtualAddress % pageSize; 
	}
	private int calculatePhysicalAddress(int virtualAddress, int ptr, int pageNum){
		int frameNum = memory.entryTables.get(ptr).frameNumber; 
		return virtualAddress + frameNum  * this.pageSize ;
	}
	private int calculatePageNumber(int virtualAddress){
		return virtualAddress/ pageSize;
	}
	private int findEntry(int virtualAddress, int pid){
		int a = -1 ;
		ArrayList<Integer> tmp =  globalBlock.pointerTable.get(pid); 
		for(int i =0 ; i < tmp.size(); ++i){
			if(virtualAddress== memory.entryTables.get(tmp.get(i)).virtualAddress){
				a = i ; // got index
				break;
			}
		}
		if(a == -1 ){
			//return -1
			return -1; 
		}
		else{
			return a;
		}
	}
	private void printDebug(){
		//Clock: 	Free frames: 0 1 2 3 4 5 6 7 
		String r = "Clock: ";
		for(int i = 0 ; i < this.clock.size(); ++i){
			if(this.memory.refPage(this.clock.get(i)))r+="*";
			r+= this.clock.get(i)+" ";
		}
		r+= " Free frames:";
		for(int i = 0; i < this.freeFrames.size(); ++i){
			r+= this.freeFrames.get(i) + " ";
		}
		System.out.println(r);
	}
	private void printEntry(boolean readOrWrite, int virtualAddress, String status, int pid){
		//R/W: R; VA: 17; Page: 0; Offset: 17; Free; Frame: 0 PA: 17
		if(this.debugOutput)this.printDebug();
		String r = "R/W: ";
		if(readOrWrite)r += "R";
		else r+= "W";
		r+= "; VA: "+virtualAddress; 
		r+= "; Page: " + this.calculatePageNumber(virtualAddress);
		r+= "; Offset: "+ this.calculateOffset(virtualAddress);
		r+= "; Status: "+ status; 
		r+= "; Frame: "+ this.calculatePhysicalAddress(virtualAddress, this.findEntry(virtualAddress, pid), this.calculatePageNumber(virtualAddress));
		System.out.println(r);
	}
	public void endProcess(int pid){
		this.globalBlock.pointerTable.remove(pid);
		System.out.println(pid+" Ending now...");
	}
	private void cleanGlobalBlock(){
		for(int i = 0 ; i < this.memory.pidHistory.size(); ++i){
			int tmpPid = this.memory.pidHistory.get(i); 
			ArrayList<Integer> tmpPageIndex = this.globalBlock.pointerTable.get(tmpPid);
			for(int j = 0; j < tmpPageIndex.size(); ++j){
				if(this.memory.entryTables.get(j)==null)this.globalBlock.pointerTable.get(tmpPid).remove(j);
			}
		}
	}
	private void pageFaultHandle(){
		//First check if there is actually more than one thing in the table
		if(memory.entryTables.size() <=1)return ; //just do nothing
		else{
			//First check what hand is pointing to, if 0, then free it else set to 0
			if(this.memory.pageTable.get(removalTick).referenced){
				this.memory.removalOfEntry(removalTick);
				this.cleanGlobalBlock();
				this.freeFrames.add(removalTick);
			}
			//move hand forward
			this.removalTick++;
			if(this.removalTick > this.maxPages)removalTick =0 ;
		}
	}
	public boolean read(int virtualAddress, int pid){
		//Is it in there already?
		int ptr = this.findEntry(virtualAddress, pid);
		//Check if its there first
		if(ptr == -1){
			int pn = this.calculatePageNumber(virtualAddress);
			//memory location not found to be allocated 
			int frameNumber = calculatePageNumber(virtualAddress);
			pageTableEntry e = new pageTableEntry(virtualAddress, frameNumber, pn, true , false, false);
			memory.entryTables.add(e);
			//This also means a pagefault so we call pagefault guy
			this.pageFaultHandle();
			if(this.freeFrames.contains(pn))this.freeFrames.remove(pn);
			if(this.clock.contains(pn) == false)this.clock.add(pn);
			return false;
		}
		else{
			int status = this.pageType(pid, virtualAddress);
			if(status ==-1){
				//Page is free so give it a new frame within that page
				//Several Responsibilities 
				//Allocate entry on both globalBlock, and the block that is for processes 
				//Set initial values for freed block 
				int pn = this.calculatePageNumber(virtualAddress);
				pageTableEntry p = new pageTableEntry(virtualAddress, currentFrame, pn, true, false, false);
				this.globalBlock.pointerTable.get(pid).add(memory.entryTables.size());
				this.memory.entryTables.add(p);
				this.printEntry(true,virtualAddress, "Free", pid );
				this.memory.pidHistory.add(pid);
				this.incrementFrames();
				this.incrementPageNumber();
				if(this.freeFrames.contains(pn))this.freeFrames.remove(pn);
				if(this.clock.contains(pn) == false)this.clock.add(pn);
				return false;
			}
			else if(status ==0){
				//Page is valid
				this.printEntry(true, virtualAddress, "Hit!!", pid );
				//So set it as clean
				this.memory.entryTables.get(ptr).valid = false;
				this.memory.entryTables.get(ptr).dirty = true;
				this.setPenalty(pid, ptr);
				return true;
			}
			else if(status ==1){
				//Page is clean
				//Engage wait
				this.printEntry(true, virtualAddress, "Clean", pid );
				this.memory.entryTables.get(ptr).penaltyDec();
				return false;
			}
			else if(status ==2){
				//Page is dirty
				//Engage wait 
				this.printEntry(true, virtualAddress, "Dirty", pid );
				this.memory.entryTables.get(ptr).penaltyDec();
				return false;
			}
			else{//Status == 3
				//Page is serving time for crimes
				this.printEntry(true, virtualAddress, "Waiting", pid );
				return false;
			}
		}
	}
	public boolean write(int virtualAddress, int pid){
		//only thing that changes really is that we now set a dirty flag after hit
		//Is it in there already?
		int ptr = this.findEntry(virtualAddress, pid);
		//Check if its there first
		if(ptr == -1){
			//memory location not found to be allocated 
			int pn = this.calculatePageNumber(virtualAddress);
			int frameNumber = calculatePageNumber(virtualAddress);
			pageTableEntry e = new pageTableEntry(virtualAddress, frameNumber, pn, true , false, false);
			memory.entryTables.add(e);
			//This also means a pagefault so we call pagefault guy
			this.pageFaultHandle();
			if(this.freeFrames.contains(pn))this.freeFrames.remove(pn);
			if(this.clock.contains(pn) == false)this.clock.add(pn);
			return false;
		}
		else{
			int status = this.pageType(pid, virtualAddress);
			if(status ==-1){
				//Page is free so give it a new frame within that page
				//Several Responsibilities 
				//Allocate entry on both globalBlock, and the block that is for processes 
				//Set initial values for freed block 
				int pn = this.calculatePageNumber(virtualAddress);
				pageTableEntry p = new pageTableEntry(virtualAddress, currentFrame, pn, true, false, false);
				this.globalBlock.pointerTable.get(pid).add(memory.entryTables.size());
				this.memory.entryTables.add(p);
				this.printEntry(true,virtualAddress, "Free", pid );
				this.memory.pidHistory.add(pid);
				this.incrementFrames();
				this.incrementPageNumber();
				if(this.freeFrames.contains(pn))this.freeFrames.remove(pn);
				if(this.clock.contains(pn) == false)this.clock.add(pn);
				return false;
			}
			else if(status ==0){
				//Page is valid
				this.printEntry(true, virtualAddress, "Hit!!", pid );
				//So set it as clean
				this.memory.entryTables.get(ptr).valid = false;
				this.setPenalty(pid, ptr);
				return true;
			}
			else if(status ==1){
				//Page is clean
				//Engage wait
				this.printEntry(true, virtualAddress, "Clean", pid );
				this.memory.entryTables.get(ptr).penaltyDec();
				return false;
			}
			else if(status ==2){
				//Page is dirty
				//Engage wait 
				this.printEntry(true, virtualAddress, "Dirty", pid );
				this.memory.entryTables.get(ptr).penaltyDec();
				return false;
			}
			else{//Status == 3
				//Page is serving time for crimes
				this.printEntry(true, virtualAddress, "Waiting", pid );
				return false;
			}
		}
		
	}
}

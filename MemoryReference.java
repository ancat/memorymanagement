/**
 *  The memory reference object. It maps addresses in memory to
 *  the type of access. A "reference" value of true refers to 
 *  an address being read from. False means being written to.
 */

public class MemoryReference {
	public int address;
	public boolean reference;
	public MemoryReference(int address, boolean reference) {
		this.address = address;
		this.reference = reference;
		Debug.print("I am " + (this.reference ? "reading" : "writing" ) + " to " + this.address);
	}
}
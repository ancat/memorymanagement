public class Frame {
	public PageEntry entry;
	public int penalty;
	public Frame (PageEntry entry, int penalty) {
		this.entry = entry;
		this.penalty = penalty;
	}

	public Frame() {}
}
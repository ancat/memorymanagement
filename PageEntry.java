/**
 *  The entries that go in the page table. We can keep track here
 *  whether a page has been written to, is in use, etc. There is 
 *  not much to this class. It's pretty much the Java equivalent
 *  a struct in C.
 */

public class PageEntry {
	public boolean valid;
	public boolean dirty;
	public boolean referenced;
	public boolean busy;
	public int pageNumber;
	public int frameNumber;

	public PageEntry(int pageNumber, int frameNumber, boolean valid, boolean dirty, boolean referenced, boolean busy) {
		this.pageNumber = pageNumber;
		this.frameNumber = frameNumber;
		this.valid = valid;
		this.dirty = dirty;
		this.referenced = referenced;
		this.busy = busy;
	}

	public PageEntry(int pageNumber, int frameNumber, boolean valid, boolean dirty, boolean referenced) {
		this(pageNumber, frameNumber, valid, dirty, referenced, false);
	}
}
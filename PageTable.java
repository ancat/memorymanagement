/**
 *  This is the Page Table, where all the Page Entries are stored.
 */

import java.util.ArrayList;

public class PageTable {
	public int pageSize;
	public int numPages;
	public ArrayList<PageEntry> pageTable;

	public PageTable(int pageSize, int numPages) {
		this.pageSize = pageSize;
		this.numPages = numPages;
		this.pageTable = new ArrayList<PageEntry>();
		for (int i = 0; i < this.numPages; i++) {
			this.pageTable.add(new PageEntry(i, 0, false, false, false));
		}
	}

	public PageEntry getEntryByNumber(int number) {
		return this.pageTable.get(number);
	}

	public PageEntry getEntryByAddress(int address) {
		return this.pageTable.get(address/this.pageSize);
	}
}
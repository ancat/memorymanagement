
public class pageTableEntry {
	//These are metadata on the memory locations
	//public boolean free; //>implied value of !valid && !dirty 
	public boolean valid;
	public boolean dirty; 
	public boolean referenced;
	public boolean busy;
	public int pageNumber;
	public int frameNumber;
	public int virtualAddress;
	public int penalty;
	public pageTableEntry(int virtualAddress, int frameNumber, int pageNumber, boolean valid, boolean referenced, boolean dirty) {
		this.valid			= valid;
		this.referenced		= referenced;
		this.dirty			= dirty;
		this.frameNumber	= frameNumber;
		this.pageNumber		= pageNumber;
		this.virtualAddress = virtualAddress;
		this.busy			= false;
	}
	public void setValid(){
		this.valid = true;
	}
	public void setAsInvalid(){
		this.valid = false;
	}
	public void setDirty(){
		this.dirty = true;
	}
	public void setAsNotDirty(){
		this.dirty = false;
	}
	public void penaltyDec(){
		this.penalty--;
		this.busy = true;
		if(penalty <= 0 ){
			this.busy  = false;
			this.dirty = false;
			this.valid = true;
		}
	}
}

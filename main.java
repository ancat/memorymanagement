/**
 *  The main file.
 *  It loads the parsed configs and processes, and "runs" them.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.ArrayList;

public class main {
    public Process current = null;
    public ArrayList<Process> processes;
    public int tick = 0;
    public Config config;

    public boolean done() {
        if (this.current != null) {
            return false;
        } else if (this.processes.size() != 0) {
            return false;
        }

        return true;
    }

    public void run() {
        MMU manager = new MMU(this.config.missPenalty, this.config.dirtyPagePenalty, this.config.pageSize, this.config.numPages);
        // Begin running the processes
        while (!this.done()) {
            // update the clock
            tick++;
            manager.tick();
            Debug.print(String.format("Tick %d", tick));

            if (this.current == null) {
                // Select one process to run
                this.current = this.processes.remove(0);
                Debug.print("Running " + this.current.pid);
            } {
                // If we're done with a process, remove it and free up its memory
                if (this.current.done()) {
                    Debug.print("Quitting: " + this.current.pid);
                    manager.free(this.current);
                    this.current = null;
                    continue;
                }
            }

            // check if the current process can access the page it wants
            MemoryReference reference = this.current.references.get(0);
            PageEntry entry = this.current.pageTable.getEntryByAddress(reference.address);

            // if it can, let it run
            if (manager.access(entry, reference)) {
                this.current.run();
            } else {
                // we can't run
                // put it back in the list
                processes.add(this.current);
            }
        }
    }

    public main(Config config) {
        this.processes = config.processes;
        this.config = config;
    }

    public static void main(String[] args) {
        // Parse Config
        Config config;

        if (args.length > 0) {
            config = new Config(args[0]);
        } else {
            config = new Config();
        }

        for (int i = 0; i < config.processes.size(); i++) {
            Debug.print("Process " + config.processes.get(i).pid + " is lame");
        }

        main m = new main(config);
        m.run();
    }
}
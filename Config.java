/**
 *  The object that handles parsing of the config file and the
 *  processes. It holds the parsed config file and an arraylist
 *  of parsed processes.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.ArrayList;

class Config {
	public String referenceFile;
	public int missPenalty;
	public int dirtyPagePenalty;
	public int pageSize;
	public int numPages;
	public int VAbits;
	public int PAbits;
	public ArrayList<Process> processes;
	public Config() {
		this("MemoryManagement.txt");
	}

	public Config(String configfile) {
		File file = new File(configfile);
		try {
			Scanner scanner = new Scanner(file);
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] pair = line.split("=");
				if (pair[0].equals("referenceFile")) {
					this.referenceFile = pair[1];
				} else if (pair[0].equals("missPenalty")) {
					this.missPenalty = Integer.parseInt(pair[1]);
				} else if (pair[0].equals("dirtyPagePenalty")) {
					this.dirtyPagePenalty = Integer.parseInt(pair[1]);
				} else if (pair[0].equals("pageSize")) {
					this.pageSize = Integer.parseInt(pair[1]);
				} else if (pair[0].equals("VAbits")) {
					this.VAbits = Integer.parseInt(pair[1]);
				} else if (pair[0].equals("PAbits")) {
					this.PAbits = Integer.parseInt(pair[1]);
				} else if (pair[0].equals("debug")) {
					Debug.enabled = pair[1].equals("true");
				} else {
					System.out.println("I donut know! "+ pair[0]);
				}
			}
			scanner.close();
		}
		catch (FileNotFoundException e) {
			System.out.println("Sorry, we had an error.");
		}

		this.numPages = (int) Math.round(Math.pow(2, this.VAbits)/this.pageSize);
		this.loadProcesses();
	}

	public void loadProcesses() {
		File file = new File(this.referenceFile);
		this.processes = new ArrayList<Process>();
		Process process;
		int numReferences;
		String referenceString;
		int pid;
		ArrayList<MemoryReference> references;
		PageTable pageTable;

		try {
			Scanner scanner = new Scanner(file);
			int numProcesses = Integer.parseInt(scanner.nextLine());
			Debug.print("" + numProcesses + " processes found.");
			scanner.nextLine();

			for (int i = 0; i < numProcesses && scanner.hasNextLine(); i++) {
				pid = Integer.parseInt(scanner.nextLine());

				numReferences = Integer.parseInt(scanner.nextLine());
				references = new ArrayList<MemoryReference>();

				Debug.print("Currently on process " + pid);
				Debug.print("References: " + numReferences);

				for (int j = 0; j < numReferences; j++) {
					referenceString = scanner.nextLine();
					references.add(new MemoryReference(Integer.parseInt(referenceString.split(" ")[0]), referenceString.split(" ")[1].equals("R")));
				}

				process = new Process(pid, this.pageSize, numPages, references);
				processes.add(process);

				if (!scanner.hasNextLine()) {
					break;
				}
				scanner.nextLine();
			}
		} catch (FileNotFoundException e) {
			System.out.println("Sorry, we had an error.");
		}
	}

	public String toString() {
		return 
			String.format(
				"Reference File: %s\nMiss Penalty: %d\nDirty Page Penalty: %d\nPage Size: %d\nVA Bits: %d\nPA bits: %d",
				this.referenceFile,
				this.missPenalty,
				this.dirtyPagePenalty,
				this.pageSize,
				this.VAbits,
				this.PAbits
			)
		;
	}
}
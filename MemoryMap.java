
import java.util.ArrayList;

public class MemoryMap {
	public ArrayList<pageTableEntry> entryTables;	//index of location of allocations
	public ArrayList<pageInfo> pageTable;
	public ArrayList<Integer> pidHistory;
//	public MemoryMap(int frameNumber, int pageNumber,int virtualAddress, boolean valid, boolean referenced, boolean dirty){
//		pageTable.add( new pageTableEntry(virtualAddress,frameNumber,  pageNumber,  valid,  referenced,  dirty));
//	}
	public void removalOfEntry(int pageNumber){
		for(int i = 0 ; i < entryTables.size(); ++i){
			if(entryTables.get(i).pageNumber == pageNumber){
				entryTables.set(i, null);
			}
		}
	}
	public int getPage(int pageNumber){
		for (int i =0 ; i < pageTable.size(); ++i){
			if(pageTable.get(i).pageNumber== pageNumber){
				return i ; 
			}
		}
		return -1; 
	}
	public boolean refPage(int pageNumber){
		for (int i =0 ; i < pageTable.size(); ++i){
			if(pageTable.get(i).pageNumber== pageNumber){
				return pageTable.get(i).referenced;
			}
		}
		return false;
	}
}
//pid, frameNumber, pageNumber, true, false, false
